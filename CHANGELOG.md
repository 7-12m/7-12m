# 7-12m Túlz
## Letöltés
 * [Android](https://gitlab.com/7-12m/7-12m/raw/master/hu.rmg.m712.Toolz.apk)
## Változások
[Korábbi frissítések](https://github.com/opekope2/7-12m/blob/master/CHANGELOG.md)
### 2019.6
 * Pünkösd
### 2019.5
 * Osztálykirándulás nem tanítási nap
 * Feliratok frissítése
### 2019.4
 * Visszaszámláló pontosítása
### 2019.3
 * Félautomata frissítő a manuális helyett
 * Kód tisztítása és csúnyábbá tétele
 * Dátumok javítása
 * Verzió javítása
 * Új funkció előkészítése
### 2019.2
 * Számítások pontosítása
### 2019.1
 * Sötét téma
 * Új színek
 * Fölösleges ísztör egg kivétele
 * Szünetek pontosítása
 * Számítások pontosítása
 * Második idegen nyelv testreszabhatósága
 * Manuális és automata újraszámítás opció
 * Beállítások különvétele
### 2018.11
 * Sebesség optimalizálás
 * Hibajavítások
 * Ísztör egg béta
 * Automata szünetválasztási beállítás
### 2018.10
 * Javított frissítő
 * Jövőbeli összeomlás javítása
 * Szünet elállításának megakadályozása
### 2018.1
 * Frissített órarend
 * Több szünet választási lehetőség
 * Új dizájn
 * Beállítások
